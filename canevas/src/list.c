#include <stdlib.h>
#include <stdio.h>
#include "list.h"
#include "geometry.h"
/*
 * créer et renvoie un nouveau nœud (LNode)
 */
static LNode * new_node(void *data) {
	LNode * noeud=(LNode *) malloc(1*sizeof(LNode));
	noeud->data=data;
	noeud->next=NULL;
	noeud->prev=NULL;
	return noeud ;

}

/*
 * créer un élément de liste (LNode)
 * y ranger la donnée (data)
 * insérer l'élément en tête de la liste *list
 */
void list_prepend(List *list, void *data) {
	LNode * res;
	res=new_node(data);
	if (list->head == NULL){
		list->head=res;
		list->tail=list->head;
	}
	else{
		res->next=list->head;
		list->head->prev=res;
		list->head=res;
	}
	list->size +=1; 
}

/*
 * créer un élément de liste (LNode)
 * y ranger la donnée (data)
 * insérer l'élément en queue de la liste *list
 */
void list_append(List *list, void *data) {
	LNode * res;
	res=new_node(data);
	if (list->head== NULL)
	{                                                      
		list->tail=res;
		list->head=list->tail;
	}
	else
	{
		res->prev=list->tail;
		list->tail->next=res;
		list->tail=res;
	   
	}
	list->size +=1; 	    
}

/*
 * créer un élément de liste (LNode)
 * y ranger la donnée (data)
 * insérer l'élément dans la liste *list après l'élément prev
 * ce dernier est supposé appartenir effectivement à la liste
 */
void list_insert_after(List *list, void *data, LNode *curr) 
{
	LNode * noeud = new_node(data); 
	if(curr->next !=NULL)
	{ 
		noeud->next=curr->next;
		curr->next->prev=noeud;
		noeud->prev=curr;
		curr->next=noeud;     
	}
	else{
		curr->next=noeud;
		noeud->prev=curr;
		noeud->next=NULL;
		list->tail=noeud;
	}
	list->size +=1;
}

/*
 * supprimer le premier élément de la liste *list
 * si celle-ci n'est pas vide
 */
void list_remove_first(List *list) {
	if(list->head != NULL){
		LNode * noeud=list->head; 
		list->head=list->head->next;
		noeud->next=NULL;
		free(noeud);
	}  
	list->size -=1;                                           
}

/*
 * supprimer le dernier élément de la liste *list
 * si celle-ci n'est pas vide
 */
void list_remove_last(List *list) {
	if(list->tail != NULL){	
		LNode * noeud=list->tail; 
		list->tail=list->tail->prev;
		list->tail->next=NULL;
		noeud->prev=NULL;
		free(noeud);
	}  
	list->size -=1;    
}

/*
 * supprimer l'élément pointé par node de la liste *list
 * l'élément est supposé appartenir effectivement à la liste
 */
void list_remove_node(List *list, LNode *node){  
	if( node->prev==NULL){
		list->head=list->head->next;
		node->next=NULL;
		free(node);
	}
	else if(node->next ==NULL){
		list->tail=list->tail->prev;
		node->prev=NULL;
		free(node);
	}                                           
	else{
		node->prev->next=node->next;
		node->next->prev=node->prev;
		node->next=NULL;
		node->prev=NULL;
		free(node);

	}
	list->size -=1;  
}

/*
 * permute les positions des nuds curr et curr->next
 * dans la liste list
 */
void list_exchange_curr_next(List *list, LNode *curr){
	LNode * node;
	void * data;
	if (list->head != NULL){
		if (curr->next != NULL){ 
			node=curr->next;
			data=curr->data;
			curr->prev->next=curr->next;
			curr->next->prev=curr->prev;
			free(curr);
			list_insert_after(list, data, node);
		}
	}  
}

void afficher(List list){
	Segment* p;
	if(list.head == NULL){
	  printf("la liste est vide");	
	}
	else{
		printf("la taille de la liste est : %d \n ",list.size);
		while(list.head !=NULL){
			p=(Segment*)list.head->data;
			display_segment((*p));
			list.head=list.head->next;
		}
	}
}

/*
 * supprimer tous les éléments de la liste *list
 * sans pour autant supprimer leurs données (data)
 * qui sont des pointeurs
 */
void list_destroy(List *list){
 
	LNode * k=list->head;
	while( (*list).head != NULL){
		k=list->head;
		list->head=list->head->next;
		free(k);
	}
	list=NULL;
	list->size=0;
}

