#include "Main.h"
#include "algo.h"

JNIEXPORT  void  JNICALL  Java_Main_allPairs(JNIEnv *env, jobject obj , jstring s1, jstring  s2) {

const  char *string1 = (*env)->GetStringUTFChars(env, s1 , NULL);

const  char *string2 = (*env)->GetStringUTFChars(env, s2 , NULL);
   allPairs(string1,string2);

}
JNIEXPORT  void  JNICALL  Java_Main_BentleyOttmmann(JNIEnv *env, jobject obj , jstring s1, jstring  s2) {

const  char *string1 = (*env)->GetStringUTFChars(env, s1 , NULL);

const  char *string2 = (*env)->GetStringUTFChars(env, s2 , NULL);

   BentleyOttmmann(string1,string2);

}
