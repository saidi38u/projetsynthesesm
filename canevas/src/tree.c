#include <stdlib.h>
#include <stdio.h>
#include "tree.h"
#include "rational.h"
static void preorder(EventNode *node) {
	if (node != NULL) {
		display_point(node->key);
		printf("\n");
		preorder(node->left);
		preorder(node->right);
	}
}

static void inorder(EventNode *node) {
	if (node != NULL) {
		inorder(node->left);
		display_point(node->key);
		printf("\n");
		inorder(node->right);
	}
}

static void postorder(EventNode *node) {
	if (node != NULL) {
		postorder(node->left);
		postorder(node->right);
		display_point(node->key);
		printf("\n");
	}
}

// order = 0 (preorder), 1 (inorder), 2 (postprder)
void display_tree_keys(const EventTree *tree, int order) {
	switch (order) {
		case 0:
			preorder(tree->root);
			break;
		case 1:
			inorder(tree->root);
			break;
		case 2:
			postorder(tree->root);
			break;
		default:
			printf("display_tree_keys: non valid order parameter\n");
			exit(1);
	}
}

/*
 * renvoie un nouveau EventNode d'attribut
 *  <key, type, s1, s2, NULL, NULL>
 */
EventNode * new_event(Point key, int type, Segment *s1, Segment *s2) {
	EventNode * noeud =(EventNode * )malloc(1*sizeof(EventNode));
	noeud->key=key;
	noeud->type=type;
	noeud->s1=s1;
	if(type==0){
		noeud->s2=s2;
	}
	else{
		noeud->s2=NULL;
	}
	noeud->left=NULL;
	noeud->right=NULL;
	return noeud ;
}

/*
 * recherche la place dans tree du nouvel événement event
 * et l'insère.
 * L'ABR est modifié et la modification peut porter sur sa racine.
 */
void insert_event(EventTree *tree, EventNode *event) {
	EventNode * tr= (EventNode *) malloc(1*sizeof(EventNode));
	EventTree * tree1 = (EventTree *) malloc(1*sizeof(EventTree ));
	tree1->root=tree->root;
	tr=tree->root;
	if( tr == NULL){
		tree->root=event;
	tree->size=1;	  
	}
	else{   
		tree1->root=tree->root;
		tree1->size=tree->size;
		while( tr != NULL){
			if(point_prec( tr->key,event->key)){
				tree1->root=tr;
				tr=tr->right;   
			}
			else{   
				tree1->root=tr;
				tr=tr->left;    
			}
		}
	}
	tr=event;
	tree1->root=event;
	tree->size=tree->size +1; 
}

/*
 * trouve le prochain événement, le supprime de l'arbre et le renvoie
 */

EventNode* get_next_event(EventTree *tree) {
	if ( tree->root == NULL ){
		return NULL;
	}
	else{
		EventTree* tr= (EventTree *) malloc(1*sizeof( EventTree));
		EventNode* trD= (EventNode *) malloc(1*sizeof(EventNode ));
		EventNode * trG= (EventNode *) malloc(1*sizeof(EventNode ));
		trD=tree->root->right;
		trG=tree->root->left;
		if( ( tree->root->right==NULL ) && (tree->root->left == NULL) ){
			trD=tree->root;
			tree->root=NULL;
			tree->size=0;
			return trD;
		}
		else if ( (tree->root->left==NULL) ){
			trD=tree->root;
			tree->root->right=NULL;
			tree->root=trD;
			tree->size=tree->size-1;  
			return trD; 
		}
		else{
			tr=tree;
			trG=tree->root->left;
			while( trG->left != NULL){
				tr->root=trG;
				trG=trG->left; 
			}
			tr->root->left=NULL;
			tree->size=tree->size-1; 
			return trG;
		}
	}
}

/*
 * renvoie 1 si le clef key existe dans l'ABR tree, O sinon
 */
int event_exists(EventTree *tree, Point key) {
	EventNode* tr= (EventNode *) malloc(1*sizeof(EventNode));
	int answer=0;
	tr=tree->root;
	while( (tr != NULL) && (answer == 0) ){
		if (comparerPoint (tr->key,key)==1){
			answer=1;
		} 
		else if(point_prec( tr->key ,key)==1){ 
			tr=tr->right;
			tree->size=tree->size -1; 
		}
		else{
			tr=tr->left;
			tree->size=tree->size -1; 
		}
	}
	return answer; 
}

int comparerPoint (Point  p1,Point  p2)
{   
	return (eq(p1.x,p2.x) && eq(p1.y,p2.y)) ;
}
