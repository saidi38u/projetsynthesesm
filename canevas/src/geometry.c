#include <stdio.h>
#include <stdlib.h>
#include "geometry.h"
#include "rational.h"
/*
 * affiche le point p
 */
void display_point(const Point p) {
	fprintf(stderr,"(");
	display_rational(p.x);
	fprintf(stderr,",");
	display_rational(p.y);
	fprintf(stderr,")\n");
}

/*
 * affiche le segment s
 */
void display_segment(const Segment s) {
	fprintf(stderr,"from: ");
	display_point(s.begin);
	fprintf(stderr," to: ");
	display_point(s.end);
}

/*
 * renvoie 1 si le point d'intersection key1 précède le point
 * d'intersection key2, 0 sinon
 */
int point_prec(Point key1, Point key2) {
	if(eq(key1.x,key2.x)){
		return gt(key1.y,key2.y);
	}
	else{
		return gt(key1.x,key2.x);
	}
}

/*
 * renvoie 1 si s1 précède s2, 0 sinon
 */
int seg_prec(Segment s1, Segment s2, Rational x) {
	Point *a;
	Point *b;
	Segment segX;
	segX.begin.x=x;
	segX.end.x=x;
	segX.begin.y.num=0;
	segX.begin.y.den=1;
	segX.end.y.num=1000;
	segX.end.y.den=1;
	a = getIntersectionPoint(s1,segX);
	b = getIntersectionPoint(s2,segX);
	return gt(a->y,b->y);
}
/*
 * renvoie 1 si s1 et s2 ont une intersection, 0 sinon
 */
/*
 * renvoie 1 si s1 et s2 ont une intersection, 0 sinon
 */
 int intersect(Segment s1, Segment s2) {
	Rational codir1, codir2, a,b;
	Point intersection;
	//calcul des coef
    	codir1 = rdiv(rsub(s1.end.y,s1.begin.y),rsub(s1.end.x,s1.begin.x));
	codir2 = rdiv(rsub(s2.end.y,s2.begin.y),rsub(s2.end.x,s2.begin.x));

	//si non paralleles	
	if (eq(codir1,codir2)==0) 
	{
		//calcul de l'intersection
		a = rsub(s1.begin.y,rmul(codir1,s1.begin.x));
		b = rsub(s2.begin.y,rmul(codir2,s2.begin.x));
		intersection.x = rdiv(rsub(b,a),rsub(codir1,codir2));
		intersection.y = rmul(codir1,radd(intersection.x,a));
		
		//abscisse intersection appartient à s1
		if (( gt(intersection.x,min(s1.begin.x,s1.end.x))) && (lt(intersection.x,max(s1.begin.x,s1.end.x))))
		{
			//abscisse intersection appartient à s2
			if (( gt(intersection.x,min(s2.begin.x,s2.end.x))) && (lt(intersection.x,max(s2.begin.x,s2.end.x)))){
				return 1;
			}
		}
	}
	return 0;
}    

/*
 * calcule et renvoie le point d'intersection entre s1 et s2
 * on suppose que ce point existe
 */
Point* getIntersectionPoint(Segment s1, Segment s2) {
	Point *inter = (Point*)calloc(1,sizeof(Point));
	Rational codir1, codir2;
	Rational a, b;

	codir1 = rdiv(rsub(s1.end.y,s1.begin.y),rsub(s1.end.x,s1.begin.x));
	a = rsub(rmul(codir1,s1.begin.x),s1.begin.y);
	codir2 = rdiv(rsub(s2.end.y,s2.begin.y),rsub(s2.end.x,s2.begin.x));
	b = rsub(rmul(codir2,s2.begin.x),s2.begin.y);

	inter->x = rdiv(rsub(a,b),rsub(codir1,codir2));
	inter->y = radd(rmul(codir1,inter->x),a);
	
	return inter;
}
