#include <stdio.h>
#include <stdlib.h>
#include "algo.h"
#include "list.h"
#include "tree.h"
/*
 * ranger dans une liste les segments stockés
 * dans le fichier texte de nom infilename
 * dont la première ligne est un entier indiquant
 * le nombre de segments et les lignes suivantes
 * les coordonnées des segments.
 * chaque ligne est composée de 2 couples.
 * Le premier couple est les coordonnées du début du segment
 * au format : xnum / xden ; pour l'abscisse
 * et : ynum / yden ; pour l'ordonnée.
 * Le second couple est la fin du segment
 */
List * load_segments(char *infilename) {
	List *segments = (List*) calloc(1, sizeof(List));
	FILE *fptr;
	if ((fptr = fopen(infilename,"r")) == NULL) {
		printf("Error while opening file %s.\n", infilename);
		printf("\nsalut guys jai pas pu ouvrir lr fichier\n");
		exit(1);
	}
	int size,i;
	fscanf(fptr, "%d", &size);

	for (i = 0; i < size; i++) {
		long a1, b1, c1, d1, a2, b2, c2, d2;
		fscanf(fptr, "%ld/%ld,%ld/%ld", &a1, &b1, &c1, &d1);
		fscanf(fptr, "%ld/%ld,%ld/%ld", &a2, &b2, &c2, &d2);
		Segment *s = (Segment*) malloc(sizeof(Segment));
		Point p1 = {{a1,b1},{c1,d1}};
		Point p2 = {{a2,b2},{c2,d2}};
		if (point_prec(p1, p2)) {
			(*s).begin = p1;
			(*s).end = p2;
		}
		else {
			(*s).begin = p2;
			(*s).end = p1;
		}
		list_append(segments, s);
	}
	fclose(fptr);
	return segments;
}

/*
 * ranger dans un fichier texte de nom outfilename
 * les points de la liste *intersections.
 * La premier ligne indique le nombre de points.
 * Puis chaque ligne contient les coordonnée de chaque point.
 * Le format des coordonnées est le même que pour les segments.
 */
void save_intersections(char * outfilename, List * intersections) {
	FILE *fptr=fopen(outfilename,"w");
	int i;
	if (fptr != NULL) {
		fprintf(fptr, "%d\n", intersections->size);
		LNode *curr = intersections->head;
		for (i = 0; i < intersections->size; i++) {
			fprintf(fptr, "%ld/%ld,%ld/%ld\n",((Point*) curr->data)->x.num,((Point*) curr->data)->x.den,((Point*) curr->data)->y.num,((Point*) curr->data)->y.den);
    			curr = curr->next;
		}
	}
	else
	{
		printf("Error while opening file %s.\n", outfilename);
		exit(1);
	}

}





void allPairs(char *infilename, char *outfilename) {

	List * list = load_segments(infilename);
	LNode *L1 ;
	List *Linter =(List *) malloc(1* sizeof(List));
	Linter->head=NULL;
	Linter->size=0;
	while(list->head != NULL)
	{
		L1=list->head->next;
		while( L1 != NULL) {  
			Segment *s1;
			Segment *s2;
			s1=(Segment *)list->head->data;
			s2=(Segment*)L1->data;

            if(intersect((*s1),(*s2)))
            {
				Point * p=getIntersectionPoint((*s1),(*s2) );
                list_prepend(Linter, (void *)p);
            }
			L1=L1->next;    
		}
		list->head= list->head->next;    
	}

	save_intersections(outfilename, Linter);
}



//TODO


void GestionDebutEvent(EventTree * listPrio, EventNode * event, List * etat){
	int continu = 1;
	if(etat->size == 0){
		list_append(etat,event->s1);
	}
	else{
		Segment * seg = (Segment *)malloc(sizeof(Segment));
		seg = event->s1;
		
		Segment * segList = (Segment *)malloc(sizeof(Segment));
		LNode * N = (LNode *)malloc(sizeof(LNode));
		
		N = etat->head;
		Rational x = event->key.x;
		
		segList = (Segment*)N->data;
		
		Point* pActuel;
		if(seg_prec(*seg,*segList,x)){
			list_prepend(etat,seg);
			if(intersect(*seg,*segList)){
				pActuel=getIntersectionPoint(*seg,*segList);
				if(!(event_exists(listPrio,*pActuel))){
					insert_event(listPrio,new_event(*pActuel,0,seg,segList));
				}
			}
		}
		else{
			LNode * NPre = (LNode *)malloc(sizeof(LNode));
			Segment * S = (Segment *)malloc(sizeof(Segment));
			Segment * SPre = (Segment *)malloc(sizeof(Segment));
			NPre = etat->head;
			N = N->next;
			while(N != NULL && continu == 1){
				segList = (Segment*)N->data;
				if(seg_prec(*seg,*segList,x)){
					continu = 0;
				}
				else{
					NPre = N;
					N = N->next;
				}
			}

			list_insert_after(etat,seg,NPre);
			SPre=(Segment*)NPre->data;
			S=(Segment*)N->data;

			if(intersect(*seg,*SPre)){
				pActuel = getIntersectionPoint(*seg,*SPre);
				if(!(event_exists(listPrio,*pActuel))){
					insert_event(listPrio,new_event(*pActuel,0,seg,SPre));
				}
			}
			if(N != NULL){
				if(intersect(*seg,*S)){
					pActuel=getIntersectionPoint(*seg,*S);
					if(!(event_exists(listPrio,*pActuel))){
						insert_event(listPrio,new_event(*pActuel,0,seg,S));
					}
				}
			}
		}
	}
}


//TODO


void GestionFinEvent(EventTree * listPrio, EventNode * event, List * etat){
	int continu = 1;
	Point pFin;
	pFin = event->key;
	Segment *sActuel;
	Point* pActuel;
	
	LNode * N = (LNode *)malloc(sizeof(LNode));
	N = etat->head;
	
	LNode * NAvant = (LNode *)malloc(sizeof(LNode));
	NAvant = NULL;
	
	while(N != NULL && continu == 1){
		sActuel = (Segment*)N->data;
		(*pActuel) = sActuel->end;
		if(eq(pActuel->x,pFin.x) && eq(pActuel->y,pFin.y)){
			continu = 0;
		}else{
			NAvant = N;
			N = N->next;
		}
	}
	if(N != NULL && NAvant != NULL && N->next != NULL){

		Segment * SPre = (Segment *)malloc(sizeof(Segment));
		SPre=(Segment*)NAvant->data;
		Segment * S = (Segment *)malloc(sizeof(Segment));
		N=N->next;
		S=(Segment*)N->data;

		if(intersect(*SPre,*S)){
			pActuel=getIntersectionPoint(*SPre,*S);
			if(!(event_exists(listPrio,*pActuel))){
				insert_event(listPrio,new_event(*pActuel,0,SPre,S));
			}
		}
	}
	if(N != NULL){
		list_remove_node(etat,N);
	}
}

//TODO

//renvoie 1 si le point P est dans la liste inters
int ListePossedePoint(List * inters, Point p){
	Point* pActuel;
	LNode * N = (LNode *)malloc(sizeof(LNode));
	N = inters->head;
	
	while(N != NULL){
		pActuel = (Point *)N->data;
		if((eq(pActuel->x,p.x) && eq(pActuel->y,p.y))){
			return 1;
		}
		N = N->next;
	}
	return 0;
}

//TODO

void GestionIntersectionEvent(EventTree * listPrio, EventNode * event, List * etat, List * inters){
	
	Point * inter = (Point *)malloc(sizeof(Point));
	
	inter = &event->key;
	list_append(inters,inter);
	Segment S1 = *event->s1;
	Segment S2 = *event->s2;
	
	Point pDebutS1 = S1.begin;
	Point pDebutS2 = S2.begin;
	
	Point* pActuel;
	Segment *sActuel;
	int continu = 1;
	
	LNode * N = (LNode *)malloc(sizeof(LNode));
	LNode * No = (LNode *)malloc(sizeof(LNode));
	N = etat->head;
	
	while(continu && N != NULL){
		sActuel = (Segment*)N->data;
		(*pActuel)=sActuel->begin;
		if((eq(pActuel->x,pDebutS1.x) && eq(pActuel->y,pDebutS1.y)) || (eq(pActuel->x,pDebutS2.x) && eq(pActuel->y,pDebutS2.y))){
			continu = 0;
		}else{
			N = N->next;
		}
	}

	Segment * SPre = (Segment *)malloc(sizeof(Segment));
	No=N->prev;
	SPre=(Segment*)No->data;
	Segment * SNext = (Segment *)malloc(sizeof(Segment));
	No=N->next;
	SNext=(Segment*)No->data;
	Segment * SNN = (Segment *)malloc(sizeof(Segment));
	No=N->next;
	No=No->next;
	SNN=(Segment*)No->data;
	Segment * S = (Segment *)malloc(sizeof(Segment));
	No=N->prev;
	S=(Segment*)No->data;

	if(N->prev != NULL){
		if(intersect(*SPre,*SNext)){
			pActuel=getIntersectionPoint(*SPre,*SNext);
			if(!(event_exists(listPrio,*pActuel)) && !(ListePossedePoint(inters,*pActuel))){
				insert_event(listPrio,new_event(*pActuel,0,SPre,SNext));
			}
		}
	}
	if(N->next->next != NULL){
		if(intersect(*S,*SNN)){
			pActuel=getIntersectionPoint(*SPre,*SNN);
			if(!(event_exists(listPrio,*pActuel)) && !(ListePossedePoint(inters,*pActuel))){
				insert_event(listPrio,new_event(*pActuel,0,S,SNN));
			}
		}
	}
	if(N != NULL){
		list_exchange_curr_next(etat,N);
	}
	
}

//TODO


void BentleyOttmmann(char *infilename, char *outfilename) {
    List *LS = (List *)malloc(sizeof(List));
    LS = load_segments(infilename);
    
    List *etat = (List *)malloc(sizeof(List));
    List *inter = (List *)malloc(sizeof(List));
     
    etat->size = 0;
    etat->head = NULL;
   
    inter->size = 0;
    inter->head = NULL;
   
    EventTree *listprio = (EventTree *)malloc(sizeof(EventTree));
    listprio->size = 0;
    listprio->root = NULL;
     
    EventNode *event;
   
    Segment *seg = (Segment *)malloc(sizeof(Segment));


    while(LS->head != NULL){
        seg = (Segment*)LS->head->data;
        event = new_event(seg->begin,1,seg,NULL);
        insert_event(listprio,event);
        event = new_event(seg->end,2,seg,NULL);
        insert_event(listprio,event);
	list_remove_first(LS);

    }

    while(listprio->size > 0){
	event = get_next_event(listprio);
		if(event->type == 0){
			GestionIntersectionEvent(listprio,event,etat,inter);
        }
        else if(event->type == 1){
			GestionDebutEvent(listprio,event,etat);
            }
    	else if(event->type == 2){
			GestionFinEvent(listprio,event,etat);
        }
    }
    save_intersections(outfilename,inter);
}
