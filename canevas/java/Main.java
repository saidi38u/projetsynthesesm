
package application;
	
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;


public class Main extends Application {
  //j'ai fais toute les �tapes mais �a marche pas bien
	public native static void  allPairs(String string, String string2);
    public native static void BentleyOttmman(String s1,String s2);
    static {
    	 System . loadLibrary ("exemple");
     }
    public String input="../data/input";
    public String output="../data/output";
	@Override
	public void start(Stage primaryStage) {
	  try {
			HBox  root = new HBox();
			root.getStyleClass().addAll("vbox","imageview");
			VBox v= new VBox();
			VBox v1=new VBox(150);
			root.setSpacing(100);
			root.getChildren().addAll(v,v1);
			Scene scene = new Scene(root,800,800);
			root.setSpacing(15);
			root.setPadding(new Insets(5));
			//pour afficher l'image en v1(vbox)
			Circle circle = new Circle(80);
	        circle.setTranslateX(10);
	        circle.setTranslateY(8);
	        circle.setCenterX(30);
	        circle.setCenterY(30);
	        Image img=new Image("hi.gif");
	        circle.setFill(new ImagePattern(img, 0.2, 0.2, 0.4, 0.4, true));
			Text text=new Text("Welcome");
	        primaryStage.setTitle("Interface G�ometie");
	        //le bouton browse
			Button browse=new Button("Browse");
			browse.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
				@Override 
				public void handle(ActionEvent event) { 	
					FileChooser file=new FileChooser();
					File f=file.showOpenDialog(null); 
						try{
							InputStream ips=new FileInputStream(f); 
							InputStreamReader ipsr=new InputStreamReader(ips);
							BufferedReader br=new BufferedReader(ipsr);
							String  ligne=br.readLine();
							ligne=br.readLine();
							while (ligne !=null){
								System.out.println(ligne);
								String [] st = ligne.split(" ");
								String[] sp1=st[0].split(",");
								String[] ent1=sp1[0].split("/");	
								double x1=Double.parseDouble(ent1[0])/ Double.parseDouble(ent1[1]);
								String[] ent2=sp1[1].split("/");	
								double y1=Double.parseDouble(ent2[0])/Double.parseDouble(ent2[1]);
								String[] sp2=st[1].split(",");
								String[] ent3=sp2[0].split("/");	
								double x2=Double.parseDouble(ent3[0])/Double.parseDouble(ent3[1]);
								String[] ent4=sp2[0].split("/");	
								double y2=Double.parseDouble(ent4[0])/Double.parseDouble(ent4[1]);
								 Line l = new Line(x1*50,y1*50,x2*50+50,y2*50+50);   
							     l.setStroke(Color.BLACK);
								 l.setStrokeWidth(1); 
								 v.getChildren().add(l);
				                 ligne=br.readLine();
							}
							br.close(); 
							
						}		
						catch (Exception e){
							System.out.println(e.toString());
						}}});
			//choice box
		ObservableList<String> variableChoisie = FXCollections.observableArrayList("Allpairs", "BentleyOtmman");
        ChoiceBox choiceBox = new ChoiceBox<String>(variableChoisie);  
	   choiceBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
     
				@Override
				public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				        String   choix=newValue;
				 
				       if(choix.equals("Allpairs"))
				        {
				    	 
				            Main.allPairs(input,output);
				        	
				        }
				        else
				        {
				        	Main.BentleyOttmman(input,output);
				        }
				}	
			
	        });
	   //le bouton run
	Button run=new Button("run");
	 run.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
               public void handle(ActionEvent event) {
					
						try{
						 
                            InputStream ips=new FileInputStream(output); 
							InputStreamReader ipsr=new InputStreamReader(ips);
							BufferedReader br=new BufferedReader(ipsr);
							String  ligne=br.readLine();
							ligne=br.readLine();
							while (ligne != null){
								String [] st = ligne.split(",");
							    String[] sp1=st[0].split("/");
								double x3=Double.parseDouble(sp1[0])/ Double.parseDouble(sp1[1]);
								String[] sp2=st[1].split("/");	
								double y3=Double.parseDouble(sp2[0])/Double.parseDouble(sp2[1]);
								Circle c = new Circle(x3*10+5,y3*10+5,5);
								c.setFill(Color.RED);
								v.getChildren().add(c);
								ligne=br.readLine();			 
							}
							br.close(); 
						}		
						catch (Exception e){
							System.out.println(e.toString());
						}}});
	 //le bouton clear
			Button clear=new Button("Clear Segment");
			clear.setOnAction(event->{v.getChildren().clear();});
	//des prop de javafx
		    browse.minWidth(100);
		    clear.minWidth(150);
		    v1.getChildren().add(text);
		    v1.getChildren().add(circle);
		    v1.getChildren().add(browse);
		    v1.getChildren().add(choiceBox);
		    v1.getChildren().addAll(run,clear);
		    text.setId("tx1");
		    v.setId("vb1");
		    v1.setId("vb2");
		    v1.setSpacing(10);
		    v.setPadding(new Insets(10));
		    v1.setSpacing(20);
		    v1.setPadding(new Insets(15));
		    text.setFill(Color.WHITE); 
		    text.setStroke(Color.BLACK); 
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setResizable(false);
			primaryStage.show();}
	       
	catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
		launch(args);
	}
}

